# Pcatalog
Pcatalog jest prostą abstrakcją na katalog osobowy, wspierany przez bazę danych. To nie jest kompletny projekt, tylko szkielet z początkowej fazy prac.
## Technologie
* Python 3.7 i wyżej
* pytest,
* mock,
* unittest,
* unittest.mock.
## Uwagi techniczne
### Preferowane IDE
PyCharm
### pytest
Przed użyciem biblioteki pytest oraz mock należy upewnić się co do wcześniejszej instalacji za pomocą modułu pip, np.: `py -m pip install pytest`.
### Uruchamianie testów
* unittest

`py -m unittest tests/unittest_catalogtest.py`
* pytest

`py -m pytest tests/pytest_catalogtest.py`